import java.io.Serializable;

public class Book implements Serializable{
	private String name;
	private String author;
	private String barCode;
	private int quantity;
	private double price;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getBarCode() {
		return barCode;
	}
	public void setBarCode(String barCode) {
		this.barCode = barCode;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public double getPrice() {
		return price;
	}
	public Book(String name, String author, String barCode, int quantity, double price) {
		super();
		this.name = name;
		this.author = author;
		this.barCode = barCode;
		this.quantity = quantity;
		this.price = price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
}
