import java.io.Serializable;

public class ScienceJournals extends Book implements Serializable{
	public int getScienceIndex() {
		return scienceIndex;
	}

	public void setScienceIndex(int scienceIndex) {
		this.scienceIndex = scienceIndex;
	}

	private int scienceIndex;

	public ScienceJournals(String name, String author, String barCode, int quantity, double price, int scienceIndex) {
		super(name, author, barCode, quantity, price);
		// TODO Auto-generated constructor stub
		this.scienceIndex = scienceIndex;
	}
}
