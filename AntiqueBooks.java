import java.io.Serializable;

public class AntiqueBooks extends Book implements Serializable{
	private int releaseYear;

	public int getReleaseYear() {
		return releaseYear;
	}

	public void setReleaseYear(int releaseYear) {
		this.releaseYear = releaseYear;
	}

	public AntiqueBooks(String name, String author, String barCode, int quantity, double price, int releaseDate) {
		super(name, author, barCode, quantity, price);
		// TODO Auto-generated constructor stub
		this.releaseYear = releaseDate;
	}

	
}
