import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<AntiqueBooks> antiques = new ArrayList<AntiqueBooks>();
		List<ScienceJournals> journals = new ArrayList<ScienceJournals>();
		List<Book> books = new ArrayList<Book>();
		Scanner scanner = new Scanner(System.in);

		try {
			String filepath = ("antiques.txt");
			FileInputStream fileIn = new FileInputStream(filepath);
			ObjectInputStream objectIn = new ObjectInputStream(fileIn);
			antiques = (List<AntiqueBooks>) objectIn.readObject();

		} catch (Exception ex) {
			// ex.printStackTrace();
		}
		try {
			String filepath = ("journals.txt");
			FileInputStream fileIn = new FileInputStream(filepath);
			ObjectInputStream objectIn = new ObjectInputStream(fileIn);
			journals = (List<ScienceJournals>) objectIn.readObject();

		} catch (Exception ex) {
			// ex.printStackTrace();
		}
		while (true) {
			System.out.println("1) Put a book into the system");
			System.out.println("2) Retrieve a book from the system");
			System.out.println("3) Update a book");
			System.out.println("4) Calculate total price of antique book");
			System.out.println("5) Calculate total price of science journal");
			System.out.println("6) Calculate total price of simple book");
			System.out.println("7) Exit");
			String choice = scanner.nextLine();
			switch (choice) {
			case "1":
				System.out.println("1) Antique Book");
				System.out.println("2) Science Journal");
				System.out.println("3) Book");
				System.out.println("Enter your choice!");
				String addChoice = scanner.nextLine();
				switch (addChoice) {
				case "1": {
					System.out.println("Enter book Name");
					String name = scanner.nextLine();
					System.out.println("Enter author Name");
					String author = scanner.nextLine();
					System.out.println("Enter barcode");
					String barcode = scanner.nextLine();
					System.out.println("Enter quantity");
					int quantity = scanner.nextInt();
					System.out.println("Enter price");
					double price = scanner.nextDouble();
					System.out.println("Enter release date");
					int date = scanner.nextInt();
					if(date <= 1900)
					{
						AntiqueBooks antique = new AntiqueBooks(name, author, barcode, quantity, price, date);
						antiques.add(antique);
						try {
							String filepath = ("antiques.txt");
							FileOutputStream fileOut = new FileOutputStream(filepath);
							ObjectOutputStream objectOut = new ObjectOutputStream(fileOut);
							objectOut.writeObject(antiques);
							objectOut.close();
							System.out.println("The Object  was succesfully written to a file");

						} catch (Exception ex) {
							ex.printStackTrace();
						}
					}
					else
					{
						System.out.println("Ancience books should have release year less than or equal to 1900");
					}
					
				}
					break;
				case "2": {
					System.out.println("Enter book Name");
					String name = scanner.nextLine();
					System.out.println("Enter author Name");
					String author = scanner.nextLine();
					System.out.println("Enter barcode");
					String barcode = scanner.nextLine();
					System.out.println("Enter quantity");
					int quantity = scanner.nextInt();
					System.out.println("Enter price");
					double price = scanner.nextDouble();
					System.out.println("Enter Science Index");
					int index = scanner.nextInt();
					if(index >= 1 && index <= 10)
					{
						ScienceJournals journal = new ScienceJournals(name, author, barcode, quantity, price, index);
						journals.add(journal);
						try {
							String filepath = ("journals.txt");
							FileOutputStream fileOut = new FileOutputStream(filepath);
							ObjectOutputStream objectOut = new ObjectOutputStream(fileOut);
							objectOut.writeObject(journals);
							objectOut.close();
							System.out.println("The Object  was succesfully written to a file");

						} catch (Exception ex) {
							ex.printStackTrace();
						}
					}
					else
					{
						System.out.println("Science index should be 1-10");
					}
					
				}
					break;
				case "3":
				{
						System.out.println("Enter book Name");
						String name = scanner.nextLine();
						System.out.println("Enter author Name");
						String author = scanner.nextLine();
						System.out.println("Enter barcode");
						String barcode = scanner.nextLine();
						System.out.println("Enter quantity");
						int quantity = scanner.nextInt();
						System.out.println("Enter price");
						double price = scanner.nextDouble();
						Book book = new Book(name, author, barcode, quantity, price);
						books.add(book);
						try {
							String filepath = ("books.txt");
							FileOutputStream fileOut = new FileOutputStream(filepath);
							ObjectOutputStream objectOut = new ObjectOutputStream(fileOut);
							objectOut.writeObject(books);
							objectOut.close();
							System.out.println("The Object  was succesfully written to a file");

						} catch (Exception ex) {
							ex.printStackTrace();
						}
					}
						break;
				}
				break;
			case "2":
				System.out.println("1) Antique Book");
				System.out.println("2) Science Journal");
				System.out.println("3) Simple Book");
				System.out.println("Enter your choice!");
				String retrieveChoice = scanner.nextLine();
				switch (retrieveChoice) {
				case "1": {
					try {
						System.out.println("Enter barcode: ");
						String barcode = scanner.nextLine();
						String filepath = ("antiques.txt");
						FileInputStream fileIn = new FileInputStream(filepath);
						ObjectInputStream objectIn = new ObjectInputStream(fileIn);
						antiques = (List<AntiqueBooks>) objectIn.readObject();
						for (int i = 0; i < antiques.size(); i++) {
							if (antiques.get(i).getBarCode().equals(barcode)) {
								System.out.println("Name: " + antiques.get(i).getName());
								System.out.println("Author: " + antiques.get(i).getAuthor());
								System.out.println("Barcode: " + antiques.get(i).getBarCode());
								System.out.println("Quantity: " + antiques.get(i).getQuantity());
								System.out.println("Price: " + antiques.get(i).getPrice());
								System.out.println("Release Year: " + antiques.get(i).getReleaseYear());
							}
						}
					} catch (Exception ex) {
						// ex.printStackTrace();
					}

				}
					break;
				case "2": {
					try {
						System.out.println("Enter barcode: ");
						String barcode = scanner.nextLine();
						String filepath = ("journals.txt");
						FileInputStream fileIn = new FileInputStream(filepath);
						ObjectInputStream objectIn = new ObjectInputStream(fileIn);
						journals = (List<ScienceJournals>) objectIn.readObject();
						for (int i = 0; i < journals.size(); i++) {
							if (antiques.get(i).getBarCode().equals(barcode)) {
								System.out.println("Name: " + journals.get(i).getName());
								System.out.println("Author: " + journals.get(i).getAuthor());
								System.out.println("Barcode: " + journals.get(i).getBarCode());
								System.out.println("Quantity: " + journals.get(i).getQuantity());
								System.out.println("Price: " + journals.get(i).getPrice());
								System.out.println("Science Index: " + journals.get(i).getScienceIndex());
							}
						}
					} catch (Exception ex) {
						// ex.printStackTrace();
					}
				}
					break;
				case "3":
				{
					try {
						System.out.println("Enter barcode: ");
						String barcode = scanner.nextLine();
						String filepath = ("books.txt");
						FileInputStream fileIn = new FileInputStream(filepath);
						ObjectInputStream objectIn = new ObjectInputStream(fileIn);
						books = (List<Book>) objectIn.readObject();
						for (int i = 0; i < books.size(); i++) {
							if (books.get(i).getBarCode().equals(barcode)) {
								System.out.println("Name: " + books.get(i).getName());
								System.out.println("Author: " + books.get(i).getAuthor());
								System.out.println("Barcode: " + books.get(i).getBarCode());
								System.out.println("Quantity: " + books.get(i).getQuantity());
								System.out.println("Price: " + books.get(i).getPrice());
							}
						}
					} catch (Exception ex) {
						// ex.printStackTrace();
					}

				}
					break;
				}
				break;
			case "3":
				System.out.println("1) Antique Book");
				System.out.println("2) Science Journal");
				System.out.println("3) Simple Book");
				System.out.println("Enter your choice!");
				String updateChoice = scanner.nextLine();
				switch (updateChoice) {
				case "1": {
					try {
						System.out.println("Enter barcode: ");
						String barcode = scanner.nextLine();
						String filepath = ("antiques.txt");
						FileInputStream fileIn = new FileInputStream(filepath);
						ObjectInputStream objectIn = new ObjectInputStream(fileIn);
						antiques = (List<AntiqueBooks>) objectIn.readObject();
						for (int i = 0; i < antiques.size(); i++) {
							if (antiques.get(i).getBarCode().equals(barcode))

							{
								System.out.println("Enter your choice to update: ");
								System.out.println("1: Book name");
								System.out.println("2: Author");
								System.out.println("3: Barcode");
								System.out.println("4: Quantity");
								System.out.println("5: Price");
								System.out.println("6: Release Year");
								String updateinnerchoice = scanner.nextLine();
								switch (updateinnerchoice) {
								case "1":
									System.out.println("Enter book Name");
									String name = scanner.nextLine();
									antiques.get(i).setName(name);
									break;
								case "2":
									System.out.println("Enter author Name");
									String author = scanner.nextLine();
									antiques.get(i).setAuthor(author);
									break;
								case "3":
									System.out.println("Enter barcode");
									String barcode1 = scanner.nextLine();
									antiques.get(i).setBarCode(barcode1);
									break;
								case "4":
									System.out.println("Enter quantity");
									int quantity = scanner.nextInt();
									antiques.get(i).setQuantity(quantity);
									break;
								case "5":

									System.out.println("Enter price");
									double price = scanner.nextDouble();
									antiques.get(i).setPrice(price);
									break;
								case "6":
									System.out.println("Enter release year");
									int date = scanner.nextInt();
									antiques.get(i).setReleaseYear(date);
									break;
								}

							}
													}
						try {
							filepath = ("antiques.txt");
							FileOutputStream fileOut = new FileOutputStream(filepath);
							ObjectOutputStream objectOut = new ObjectOutputStream(fileOut);
							objectOut.writeObject(antiques);
							objectOut.close();
							System.out.println("The Object  was succesfully updated to a file");

						} catch (Exception ex) {
							ex.printStackTrace();
						}

					} catch (Exception ex) {
						// ex.printStackTrace();
					}

				}
					break;
				case "2": {
					try {
						System.out.println("Enter barcode: ");
						String barcode = scanner.nextLine();
						String filepath = ("journals.txt");
						FileInputStream fileIn = new FileInputStream(filepath);
						ObjectInputStream objectIn = new ObjectInputStream(fileIn);
						journals = (List<ScienceJournals>) objectIn.readObject();
						for (int i = 0; i < journals.size(); i++) {
							if (journals.get(i).getBarCode().equals(barcode))

							{
								System.out.println("Enter your choice to update: ");
								System.out.println("1: Book name");
								System.out.println("2: Author");
								System.out.println("3: Barcode");
								System.out.println("4: Quantity");
								System.out.println("5: Price");
								System.out.println("6: Science Index");
								String updateinnerchoice = scanner.nextLine();
								switch (updateinnerchoice) {
								case "1":
									System.out.println("Enter book Name");
									String name = scanner.nextLine();
									journals.get(i).setName(name);
									break;
								case "2":
									System.out.println("Enter author Name");
									String author = scanner.nextLine();
									journals.get(i).setAuthor(author);
									break;
								case "3":
									System.out.println("Enter barcode");
									String barcode1 = scanner.nextLine();
									journals.get(i).setBarCode(barcode1);
									break;
								case "4":
									System.out.println("Enter quantity");
									int quantity = scanner.nextInt();
									journals.get(i).setQuantity(quantity);
									break;
								case "5":

									System.out.println("Enter price");
									double price = scanner.nextDouble();
									journals.get(i).setPrice(price);
									break;
								case "6":
									System.out.println("Enter science Index");
									int scienceIndex = scanner.nextInt();
									journals.get(i).setScienceIndex(scienceIndex);
									break;
								}

							}
							try {
								filepath = ("journals.txt");
								FileOutputStream fileOut = new FileOutputStream(filepath);
								ObjectOutputStream objectOut = new ObjectOutputStream(fileOut);
								objectOut.writeObject(journals);
								objectOut.close();
								System.out.println("The Object  was succesfully written to a file");

							} catch (Exception ex) {
								ex.printStackTrace();
							}
						}
					} catch (Exception ex) {
						// ex.printStackTrace();
					}
					
					
				}
				break;
				case "3":
				{
					try {
						System.out.println("Enter barcode: ");
						String barcode = scanner.nextLine();
						String filepath = ("books.txt");
						FileInputStream fileIn = new FileInputStream(filepath);
						ObjectInputStream objectIn = new ObjectInputStream(fileIn);
						books = (List<Book>) objectIn.readObject();
						for (int i = 0; i < books.size(); i++) {
							if (books.get(i).getBarCode().equals(barcode))

							{
								System.out.println("Enter your choice to update: ");
								System.out.println("1: Book name");
								System.out.println("2: Author");
								System.out.println("3: Barcode");
								System.out.println("4: Quantity");
								System.out.println("5: Price");
								String updateinnerchoice = scanner.nextLine();
								switch (updateinnerchoice) {
								case "1":
									System.out.println("Enter book Name");
									String name = scanner.nextLine();
									books.get(i).setName(name);
									break;
								case "2":
									System.out.println("Enter author Name");
									String author = scanner.nextLine();
									books.get(i).setAuthor(author);
									break;
								case "3":
									System.out.println("Enter barcode");
									String barcode1 = scanner.nextLine();
									books.get(i).setBarCode(barcode1);
									break;
								case "4":
									System.out.println("Enter quantity");
									int quantity = scanner.nextInt();
									books.get(i).setQuantity(quantity);
									break;
								case "5":

									System.out.println("Enter price");
									double price = scanner.nextDouble();
									books.get(i).setPrice(price);
									break;
								}

							}
													}
						try {
							filepath = ("books.txt");
							FileOutputStream fileOut = new FileOutputStream(filepath);
							ObjectOutputStream objectOut = new ObjectOutputStream(fileOut);
							objectOut.writeObject(books);
							objectOut.close();
							System.out.println("The Object  was succesfully updated to a file");

						} catch (Exception ex) {
							ex.printStackTrace();
						}

					} catch (Exception ex) {
						// ex.printStackTrace();
					}

				}
					break;
				}
				
					break;
				
					
				case "4":
					try {
						System.out.println("Enter barcode: ");
						String barcode = scanner.nextLine();
						String filepath = ("antiques.txt");
						FileInputStream fileIn = new FileInputStream(filepath);
						ObjectInputStream objectIn = new ObjectInputStream(fileIn);
						antiques = (List<AntiqueBooks>) objectIn.readObject();
						boolean flag = true;
						for (int i = 0; i < antiques.size(); i++) {
							double total_price = 0;
							if (antiques.get(i).getBarCode().equals(barcode))

							{
								flag = false;
								System.out.println("Enter Quantity: ");

								int quantity = scanner.nextInt();
								if (antiques.get(i).getReleaseYear() > 1900) {
									total_price = quantity * antiques.get(i).getPrice()
											* (2021 - antiques.get(i).getReleaseYear()) / 10;
								} else {
									total_price = quantity * antiques.get(i).getPrice();
								}
							}
							System.out.println("Total Price = " + total_price);
						}
					} catch (Exception ex) {
						// ex.printStackTrace();
					}
					break;
				case "5":
					try {
						System.out.println("Enter barcode: ");
						String barcode = scanner.nextLine();
						String filepath = ("journals.txt");
						FileInputStream fileIn = new FileInputStream(filepath);
						ObjectInputStream objectIn = new ObjectInputStream(fileIn);
						journals = (List<ScienceJournals>) objectIn.readObject();
						boolean flag = true;
						for (int i = 0; i < journals.size(); i++) {
							double total_price = 0;
							if (journals.get(i).getBarCode().equals(barcode))

							{
								flag = false;
								System.out.println("Enter Quantity: ");

								int quantity = scanner.nextInt();
								total_price = quantity * journals.get(i).getPrice() * journals.get(i).getScienceIndex();
							}
							System.out.println("Total Price = " + total_price);
						}
					} catch (Exception ex) {
						// ex.printStackTrace();
					}
					break;
				case "6":
					try {
						System.out.println("Enter barcode: ");
						String barcode = scanner.nextLine();
						String filepath = ("books.txt");
						FileInputStream fileIn = new FileInputStream(filepath);
						ObjectInputStream objectIn = new ObjectInputStream(fileIn);
						books = (List<Book>) objectIn.readObject();
						boolean flag = true;
						for (int i = 0; i < books.size(); i++) {
							double total_price = 0;
							if (books.get(i).getBarCode().equals(barcode))

							{
								flag = false;
								System.out.println("Enter Quantity: ");

								int quantity = scanner.nextInt();
								total_price = quantity * books.get(i).getPrice();
							}
							System.out.println("Total Price = " + total_price);
						}
					} catch (Exception ex) {
						// ex.printStackTrace();
					}
					break;
				case "7":
					System.exit(0);
				}
			

		}
	}

}
